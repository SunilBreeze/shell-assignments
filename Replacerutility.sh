#!/bin/bash
#Author:Sunil

# The following code receives three arguments
# 1 argv ==> Directory name
# 2 argv ==> string to be found in side every C and Java file
# 3 argv ==> new string to be replaced with old string in the original C or Java file
# and replace the old string in file with new string specified as 3rd argument

# Receive the command line arguments
dir=$1
cstr=$2
nstr=$3

# This function replaces the old string with new string 
# give at command line
function replaceFiles(){
    find $dir \( -name "*.c" -o -name "*.java" \) | xargs sed -i.bak -e "s/$cstr/$nstr/g" 
    printf "Files in \"%s\" are backed up and old string '%s' is replaces with new string '%s'.\n" $dir $cstr $nstr
}

# Check if the user provides appropriate command line arguments.
if [[ $# -ne 3 ]]; then
    echo "Please enter 3 parameters to program"
    echo "1 argv ==> Directory name"
    echo "2 argv ==> string to be found in side every C and Java file"
    echo "3 argv ==> new string to be replaced with old string in the original C or Java file"
    exit 1
else
    replaceFiles
fi