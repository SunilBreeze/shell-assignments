#!/bin/bash
#Author:Sunil

# variable declaration
declare -a names
declare -a split_names
declare -a final_array
fname=$1

    # The below function will eleminate the comment lines 
    # assuming that every comment will be in new line
    # and read only uncommented lines
function readFile (){
    let i=0
    IFS=  #stands for "internal field separator"
    while read -r line || [[ -n "$line" ]];  do 
        if [[ ! $line =~ "#" ]]; then   
            names["$i"]="$line"
            let "i++"
        fi
    done < $fname  # this is the input file
}

    # This function will split each line and we saperate First name and Last name
function splitTheNames(){
    IFS=':'
    let i=0
    for name in ${names[@]}; do
        split_names["${i}"]=$name
        let "i++"
    done
}

    # This function will switch the First name and Last name
function switchNames(){
    IFS=
    let i=0
    let j=i+1
    # This following loop will switch the First name and Last name
    for (( i=0;i<${#split_names[@]};i+=2 )); do
        final_array[$i]="${split_names[$j]}":"${split_names[$i]}"
        let "j+=2"
    done
}

    # This function will sort the names according to their Last name
function sortNames(){
    finalResult=($(for l in ${final_array[@]}; do echo $l; done | sort))
    echo $finalResult
}

    # This function will write the final result to the different file output.txt
function writeToFile(){
    for each_name in ${finalResult[@]}; do
        echo $each_name >> sample_output.txt
    done
}

# This is the order in which the functions are called 
# it also depicts the logic behind the whole program

if ! [[ $fname && -f $fname ]]; then
    echo "Please pass a file to read from"
    exit 1
else
    readFile
    splitTheNames
    switchNames
    sortNames
    writeToFile
    echo "Program executed successfully please check for sample_output.txt file."
fi