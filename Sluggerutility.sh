#!/bin/bash

# Author: Sunil

# The following code accepts two arguments - file name and an integer
# the script will generate a new file - output.txt containing 
# the log details which exceed execution limit given in 2nd argument while executing script

INPUTFILE=$1
num=$2

# This function will process the data as described by the problem statement
function processData(){
    awk -v limit=$num 'BEGIN{
        i = 0;
        while (getline < "'"$INPUTFILE"'"){
            split($0,ft," ");
            time[i]=ft[3]
            line[i]=$0
            i++
        }
        close("'"$INPUTFILE"'");

        for(j=0;j<i;j++){
            split(time[j],tf,":");
            sec[j]=tf[3]   # Get the seconds in sec array
        }

        for (k=0;k<j-1;k++){
            diff = sec[k+1] - sec[k]
            if(diff > limit){
                print line[k] > "output.txt"
            }
        }
    }' 
}

# This function desplayes error message when user does not give proper arguments to program
function error(){
    echo "Please pass two argument to the script"
    echo "1 argv ==> 'log filename'"
    echo "2 argv ==> time exceed limit (number)"
    exit 1
}

# The bellow logic will check if appropriate arguments are 
# supplied to script

re='^[0-9]+$' # Regular expression to check if second argument is number
if [[ -e $1 && $# -gt 1 ]]; then
    if ! [[ $2 =~ $re ]]; then
        error
    else
        processData
        echo "Please check for file - output.txt "
    fi
else
    error
fi