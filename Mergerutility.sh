#!/bin/bash
#Author:Sunil

# Array Declaration
declare -a stdRoll
declare -a stdScore
declare -a finalResult
declare -A stdRoll_2D
declare -A stdScore_2D
declare -A nameScore_2D
fnamesrno=$1
fnamessor=$2

# The following shell script will take two file and merges them together and 
# and creats a new file - output.txt - containing name:rollNo:score the data in the output.txt file is 
# sorted in decending order

# The following function reads the two fils passed as command line 
# argument
function readFile(){
    let i=0
    IFS=':' #stands for "internal field separator"
    while read -r line || [[ -n "$line" ]];  do 
        stdRoll["$i"]="$line"
        let "i++"
    done < $fnamesrno  # This is the input file

    let i=0
    while read -r line || [[ -n "$line" ]];  do 
        stdScore["$i"]="$line"
        let "i++"
    done < $fnamessor  # This is the input file
}

# This function split the record 
function splitRecords(){
    let i=0
    for items in ${stdRoll[@]}; do
        splitnameRoll[$i]=$items
        let "i++"
    done
    let i=0
    for items in ${stdScore[@]}; do
        splitnameScore[$i]=$items
        let "i++"
    done

}

# This function will create 2D arrays
# where stdRoll_2D will store the name with roll numbers
# stdScore_2D will store the name with scores of individual student
function create2Darray(){
    let j=0
    for (( i=0;i<"${#splitnameRoll[@]}";i+=2 )); do
        stdRoll_2D["$j",0]="${splitnameRoll[$i]}"
        stdRoll_2D["$j",1]="${splitnameRoll[$i+1]}"
        let "j++"
    done
    let j=0
    for (( i=0;i<"${#splitnameScore[@]}";i+=2 )); do
        stdScore_2D["$j",0]="${splitnameScore[$i]}"
        stdScore_2D["$j",1]="${splitnameScore[$i+1]}"
        let "j++"
    done
}

# This function will extract only scores into an array called scores
function getScore(){
    let j=0
    for (( i=1;i<${#splitnameScore[@]};i+=2)); do
        scores[$j]="${splitnameScore[$i]}"
        let "j++"
    done
}


# This function will sort the scores in decending order
function sortScore(){
    for (( i = 0; i < ${#scores[@]} ; i++ )); do
        for (( j = $i; j < ${#scores[@]}; j++ )); do
            if [ ${scores[$i]} -lt ${scores[$j]}  ]; then
                t=${scores[$i]}
                scores[$i]=${scores[$j]}
                scores[$j]=$t
            fi
        done
    done
}

# This function will rearrange the data from both the files 
# and merge in to a single array called finalResult
function rearrangeData(){
    elem="${#stdScore_2D[@]}"
    elem=$(( $elem/2 ))
    for (( i=0;i<${#scores[@]};i++ )); do
        for (( j=0;j<$elem;j++ )); do
            if [ ${scores[$i]} == ${stdScore_2D[$j,1]} ]; then
                nameScore_2D[$i,0]="${stdScore_2D[$j,0]}"
                nameScore_2D[$i,1]="${stdScore_2D[$j,1]}"
                break
            fi
        done
    done
    
    for (( i=0;i<$elem;i++ )); do
        for (( j=0;j<$elem;j++ )); do
            if [ ${nameScore_2D[$i,0]} ==  ${stdRoll_2D[$j,0]} ]; then
                finalResult[$i]="${stdRoll_2D[$j,0]}:${stdRoll_2D[$j,1]}:${nameScore_2D[$i,1]}"
            fi
        done
    done
    
}

# This function will write the finalResult array on to a new file
function writeToFile(){
    IFS=
    for items in ${finalResult[@]}; do
        echo "$items" >> sample_output.txt
    done
}

# The following statements are function calls 
# these statements also define the logic behind the program
if ! [[ $fnamesrno && -f $fnamesrno && $fnamessor && -f $fnamessor ]]; then
    echo "Please pass two files to read from"
    exit 1
else
    readFile
    splitRecords
    create2Darray
    getScore
    sortScore
    rearrangeData
    writeToFile
    echo "Script executed successfully please check for file - sample_output.txt"
    cat sample_output.txt
fi
