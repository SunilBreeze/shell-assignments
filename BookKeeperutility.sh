#!/bin/bash
#Author:Sunil

# The following script will read a file and identify the erroneous data.
# The erroneous data is then written in to a file called .log
# The log data includes the date and record containing error with information on correction to be made
# The script also separates the file in to different files for each file containing each days records

fname=$1

# The following function will read the file and do the needful task
# as described in the problem statement.

function readFile(){
    awk 'BEGIN{
        i = 0;
        while (getline < "'"$fname"'"){
            lines[i]=$0
            split($0,splitdata," ");
            if(splitdata[1] == "Date:" || splitdata[1] == "Total:"){
                datetime[i]=i
            }
            i++
        }close("'"$fname"'");
        
        # This loop will extract the lines where Data is specified
        
        for (k in datetime){
            split(lines[k],datas," ")
            split(datas[2],dmy,"-")
            if(length(dmy[1])<=3){
                logfnames[k]="_"dmy[3]dmy[2]dmy[1]
                noDays[k]=k
            }
        }        
        
        # This loop will rearrange the data, write ERRORS in .log file
        # and write the data about individual day in separate files
        
        currentdate=0
        for(k=0;k<i;k++){
            if(k==datetime[k] && k==noDays[k]){
                currentdate=k
                print lines[currentdate] > "'$fname'"logfnames[currentdate]
            }
            else{
                print lines[k] > "'$fname'"logfnames[currentdate]
                split(lines[k],pq," ");
                result = pq[2] * pq[3]
                if(result != pq[4]){
                     print lines[currentdate]": ERRONEOUS DATA: "lines[k] " \
                           (total in this record in wrong please change " pq[4] " to " result ")" > ".log"
                }
            }
        }
    }'
}

if ! [[ $fname && -f $fname ]]; then
    echo "Please pass a file to read from"
    exit 1
else
    readFile
    echo "Program executed successfully please check your folder contaning input file."
fi

